#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "matrix.h"
#include "vector.h"
#include "page_rank.h"

int main(int argc, char** argv){
  struct matrix mat;
  int res;
  float * vect_ar = malloc(sizeof (float) * 5);
  vect_ar[0] = 10;
  vect_ar[1] = 0;
  vect_ar[2] = 3;
  vect_ar[3] = -1;
  vect_ar[4] = 5;
  //float vect_array[5] = { 10, 0, 3, -1, 5 };
  struct vector vect, result,trans_result,vect_prob;

  if(argc < 2){
    perror("argv\n");
    exit(0);
  }

  vector_init_from_array(&vect, vect_ar, 5);
  matrix_init(&mat);

  res = matrix_parse_from_file(&mat, argv[1]);

  if(!res){
    return EXIT_FAILURE;
  }
  printf("%f \n",vect.tab[4]);
  matrix_display(mat);
  vector_display(vect);
  result = vector_matrix_multiply(vect, mat);
  trans_result = vector_matrix_transposed_multiply(vect,mat);
  printf("\n===============================================\n");
  printf("\t MULTIPLY \n");
  vector_display(result);
  printf("\n===============================================\n");
  printf("\t TRANSPOSED \n");
  vector_display(trans_result);
  

  

  float delta = page_rank_zero(mat,4);
  
  vector_init(&vect_prob,5);
  vect_prob.tab[0] = 1;

  float delta1 = page_rank(mat,vect_prob,0.4f);

  printf("delta: %f | delta0:%f\n",delta1,delta);

  vector_destroy(&trans_result);
  vector_destroy(&vect_prob);
  vector_destroy(&result);
  vector_destroy(&vect);
  matrix_destroy(&mat);
  

  free(vect_ar);
  return 0;
}
