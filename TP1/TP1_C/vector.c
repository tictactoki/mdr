#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "vector.h"
#include "matrix.h"

int vector_init(struct vector* vect, unsigned int len){
  vect->length = len;

  vect->tab = NULL;

  if(len > 0){
    vect->tab = calloc(len, (sizeof (float)));

    if(vect->tab == NULL){
      vect->length = 0;

      return 0;
    }
  }

  return 1;
}

int vector_init_from_array(struct vector* vect, float *array, unsigned int len){
  int i;
  

  if(vector_init(vect, len)){
    memcpy(vect->tab,array,len * sizeof(float));
    return 1;
  }

  return 0;
}

struct vector vector_init_probability(int length){

  struct vector vect;
  int i;
  float value,sum = 0;
  
  if(vector_init(&vect,length)){
    srand(time(NULL));
    for(i=0;i<length;i++){
      printf("%d\n",length);
      value = (float)(rand()%100)/100;
      sum += value;
      printf("%f %f\n",value,sum);
      if(sum < 1.0){
	vect.tab[i] = value;
      }
    }
    // la somme doit être égal à 1
    value = sum - 1.0;
    if(value != 0){
      //printf("OK");
      //TODO: revoir rand sur ce cas
      // on ajoute le reste à la dernière case
      vect.tab[length-1] += value;
    }
  }
  
  return vect;
  

}

int vector_set(struct vector* vect, int index, float value){
  if(index >= 0 && index < vect->length){
    vect->tab[index] = value;
    
    return 1;
  }
  
  return 0;
}

int vector_copy(struct vector* src, struct vector* dest){
  return vector_init_from_array(dest, src->tab, src->length);
}

struct vector vector_matrix_multiply(struct vector vect, struct matrix mat){
  struct vector result;
  int i, j;

  vector_init(&result, 0);
  
  //TODO: verifier n ou n+1
  if (vect.length != mat.n) {
    vector_destroy(&result);    
    return result;
  }
  
  vector_destroy(&result);
  vector_init(&result, vect.length);
  
  //TODO: verifier n ou n-1
  for (i = 0; i < mat.n; i++) {
    for (j = mat.L[i]; j < mat.L[i + 1]; j++) {
      //TODO: verifier l'indice de result.tab[_]
      result.tab[i] = result.tab[i] + mat.C[j] * vect.tab[mat.I[j]];
    }
  }
  
  return result;
}

struct vector vector_matrix_transposed_multiply(struct vector vect, struct matrix mat){
  struct vector result;
  int i, j;
  
  vector_init(&result, 0);
  
  //TODO: verifier n ou n+1
  if (vect.length != mat.n) {
    vector_destroy(&result);    
    return result;
  }
  
  vector_destroy(&result);
  vector_init(&result, vect.length);
  
  //TODO: verifier n ou n-1
  for (i = 0; i < mat.n; i++) {
    for (j = mat.L[i]; j < mat.L[i + 1]; j++) {
      //TODO: verifier l'indice de result.tab[_]
      result.tab[mat.I[j]] += mat.C[j] * vect.tab[i];
    }
  }
  
  return result;
}

float vector_distance(struct vector first, struct vector second){
  //TODO: calculer la distance
  float distance = 0.0f;
  float sub_value = 0.0f;
  int i;

  if(first.length == second.length){
    for(i=0; i<first.length; i++){
      sub_value = (second.tab[i] - first.tab[i]);
      distance += sub_value * sub_value;
    }
  }

  return (float)sqrt(distance);
}

void vector_display(struct vector vect){
  int i;

  printf("V = (\n");

  for(i=0; i<vect.length; i++){
    printf("\t%f\n", vect.tab[i]);
  }

  printf(")\n");
}

void vector_destroy(struct vector* vect){
  vect->length = 0;

  if(vect->tab != NULL){
    free(vect->tab);
  }
}
