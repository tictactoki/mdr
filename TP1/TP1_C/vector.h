#ifndef __VECTOR_H__
#define __VECTOR_H__

#include "matrix.h"

struct vector {
  unsigned int length;
  float* tab;
};

int vector_init(struct vector* vect, unsigned int len);

int vector_init_from_array(struct vector* vect, float * array, unsigned int len);

int vector_set(struct vector* vect, int index, float value);

int vector_copy(struct vector* src, struct vector* dest);

float vector_distance(struct vector first, struct vector second);

struct vector vector_matrix_multiply(struct vector vect, struct matrix mat);

struct vector vector_matrix_transposed_multiply(struct vector vect, struct matrix mat);

struct vector vector_init_probability(int length);

void vector_display(struct vector vect);

void vector_destroy(struct vector* vect);

#endif
