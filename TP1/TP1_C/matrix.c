#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "matrix.h"


int matrix_m_resize(struct matrix* mat, int new_length){
  if(new_length > mat->m){ 
    // +1 car tout les chemins possible et sur lui meme bugait
    //float* tmp_C = realloc(mat->C, sizeof(float) * (new_length+1));
    //int * tmp_I = realloc(mat->I , sizeof(int) * (new_length+1));
    //TODO remettre au propre pour L
    //int* tmp_L = realloc(mat->L, sizeof(int) * (new_length+1));
     
    float* tmp_C = realloc(mat->C, sizeof(float) * new_length);
    int * tmp_I = realloc(mat->I , sizeof(int) * new_length);
    //TODO remettre au propre pour L
    int* tmp_L = realloc(mat->L, sizeof(int) * (new_length+1));

    if(tmp_C == NULL || tmp_I == NULL){
      return 0;
    }
    
    mat->m = new_length;
    mat->n = new_length;
    mat->C = tmp_C;
    mat->I = tmp_I;
    mat->L = tmp_L;
  }
  
  return 1;
}

void matrix_n_resize(struct matrix* mat, int new_length){
  if(new_length > mat->n){
    int* tmp_L = realloc(mat->L, sizeof(int) * new_length);
    //    int* tmp_I = realloc(mat->I, sizeof(int) * new_length);

    if(tmp_L != NULL ){//&& tmp_I != NULL){
      mat->n = new_length;
    }

    if(tmp_L != NULL){     
      mat->L = tmp_L;
    }

    /* if(tmp_I != NULL){
      mat->I = tmp_I;
      }*/
  }
}

int matrix_init(struct matrix* mat){
  if(mat == NULL){
    return 0;
  }

  mat->n = 0;
  mat->m = 0;

  mat->C = NULL;
  mat->L = NULL;
  mat->I = NULL;

  return 1;
}

void matrix_destroy(struct matrix* mat){
  mat->n = 0;
  mat->m = 0;

  if(mat->C != NULL){
    free(mat->C);
    mat->C = NULL;
  }

  if(mat->L != NULL){
    free(mat->L);
    mat->L = NULL;
  }

  if(mat->I != NULL){
    free(mat->I);
    mat->I = NULL;
  }
}

int next_line(int fd, char** buf, size_t* length){
  int read_nb = 0, read_index, res;
  unsigned int buf_length = 50;
  char* tmp_buf = NULL;
    
  *buf = NULL;
  *length = 0;

  tmp_buf = NULL;  
   
  read_index = 0;

  while(1){
    //Agrandissement du buffer s'il ne peut plus rien stocker
    if(read_index >= *length){
      tmp_buf = (char*) realloc(*buf, *length + buf_length + 1);

      if(tmp_buf == NULL){
	if(*buf != NULL){
	  free(*buf);
	  *buf = NULL;
	}

	return 0;
      }
      
      *buf = tmp_buf;      
      *length = *length + buf_length;
    }

    res = read(fd, &((*buf)[read_index]), 1);

    if(res < 0){
      if(*buf != NULL){
	free(*buf);
	*buf = NULL;
      }

      return 0;
    }

    if(res == 0){
      break;
    }

    //printf("%c\n", (*buf)[read_index]);

    if((*buf)[read_index] == '\n'){
      break;
    }

    read_index++;
  }

  *length = read_index;
  tmp_buf = (char*) realloc(*buf, *length + 1);

  if(tmp_buf == NULL){
    if(*buf != NULL){
      free(*buf);
      *buf = NULL;
    }

    return 0;
  }  

  *buf = tmp_buf;
  (*buf)[read_index] = '\0';

  return 1;
}


void calcul_L(struct matrix *mat, int * indice_L, int counter, int counter_m){
  mat->L[*indice_L] = counter - counter_m -1;
  mat->L[++*indice_L++] = counter -1;
}

void calcul_C(struct matrix *mat,int *indice_C, int counter, int *counter_m){
  int i;
  for(i=*indice_C-1;i<counter-1;i++){
    mat->C[i] = 1.0/(float)(*counter_m);
  }
  
  *indice_C = counter;
  *counter_m = 0;
}

int matrix_copy(struct matrix* src, struct matrix* dest){
  int i;

  matrix_init(dest);

  //TODO: completer les champs manquants + verifier les formules
  dest->m = src->m;
  dest->n = src->n;

  //TODO: verifier les malloc et renvoyer les echecs

  dest->C = malloc(sizeof(float) * src->m);
  dest->I = malloc(sizeof(int) * src->m);

  for(i=0; i<src->m; i++){
    dest->C[i] = src->C[i];
    dest->I[i] = src->I[i];
  }

  //TODO: verifier n ou n+1
  dest->L = malloc(sizeof(int) * (src->n + 1));

  for(i=0; i<src->n+1; i++){
    dest->L[i] = src->L[i];
  }
}

int matrix_parse_from_file(struct matrix* mat, char* filepath){
  int fd, res;
  char* line = NULL;  
  size_t size;
  int old_from_node, from_node, counter_node_line,to_node,counter_m, counter, indice_C, indice_L;
  
  fd = open(filepath, O_RDONLY);

  if(fd == -1){
    perror("open\n");
    exit(0);
  }
  
  indice_L = counter = old_from_node = 0;
  indice_C = counter_node_line = 1;
  counter_m = -1;
  while(1){
    res = next_line(fd, &line, &size);

    if(!res){
      return 0;
    }

    if(size == 0 || line == NULL){
      break;
    }

    //TODO: Traitement de la ligne

    if(line[0] != '#' && sscanf(line, "%d\t%d", &from_node, &to_node) == 2){
      //printf("%s\n", line);
     
      matrix_m_resize(mat, (to_node + 1) * (to_node + 1));
      //  matrix_n_resize(mat, to_node * to_node);
      // valeur des colonnes
      mat->I[counter] = to_node;
      counter++;
      counter_m++;
      // si on change de noeud
      if(old_from_node != from_node){
	// savoir si une ligne contient que des 0 ou non
	counter_node_line = from_node - old_from_node;
	
	old_from_node = from_node;
	// calcul de L et C
	calcul_L(mat,&indice_L,counter,counter_m);
	calcul_C(mat,&indice_C,counter,&counter_m);
      }

      // si la ligne i contient que des 0 alors L[i] = L[i+1]
      while(counter_node_line > 1){
	calcul_L(mat,&indice_L,counter,counter_m);
	counter_node_line--;
      }

    }

    free(line);
  }
  // derniere ligne du fichier non traitée dans l algo donc ici...
  calcul_L(mat,&indice_L,counter,counter_m);
  counter_m++;
  counter++;
  calcul_C(mat,&indice_C,counter,&counter_m);
  // TODO remettre vrai size, vrai alloc
  
  mat->m = counter-1;
  printf("%d \n",indice_L);
  mat->n = indice_L;
  //printf("%d %d %d %d\n",mat->n,indice_L,counter,counter_m);
  // dernière valeur de L
  mat->L[indice_L] = mat->m;

  close(fd);
  return 1;
}

void matrix_display(struct matrix mat){
  int i;
  printf("===============================\n");
  for(i=0;i<mat.m;i++){
    printf("C[%d] = %f | I[%d] = %d \n",i,mat.C[i],i,mat.I[i]);
  }
  printf("\n==============================\n");

  for(i=0;i<mat.n+1;i++){
    printf("L[%d] = %d ",i,mat.L[i]);
  }
  printf("\n==============================\n");
	 
}



