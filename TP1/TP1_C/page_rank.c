#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "page_rank.h"
#include "matrix.h"
#include "vector.h"

void page_rank_with_zap(struct matrix mat, struct vector init, double factor){
  double zap = 1.0;

  zap = factor >= 0.1 ? factor : 0.1;
  zap = zap <= 0.2 ? zap : 0.2;

  //TODO: voir quoi renvoyer
}

float page_rank(struct matrix mat, struct vector init, float epsilon){
  struct vector current_page, next_page;
  float proba = 0;

  vector_copy(&init, &current_page);
  //init = vector_init_probability(mat.n);

  do{
    next_page = vector_matrix_transposed_multiply(current_page, mat);

    proba = vector_distance(next_page, current_page);

    //vector_destroy(&current_page);
    memcpy(&current_page,&next_page,mat.n * sizeof(float));
  } while(proba > epsilon);
  
  vector_destroy(&current_page);
  
  return proba;

  //TODO: voir quoi renvoyer
}


// avec k pas
float page_rank_zero(struct matrix mat, int k){
  
  struct vector pn,pn1;
  int i;
  float delta = 0;

  if(vector_init(&pn, mat.n)){
    pn.tab[0] = 1;
    
    for(i=0;i<k;i++){

      pn1 = vector_matrix_transposed_multiply(pn,mat);
      delta = vector_distance(pn1,pn);
      memcpy(&pn,&pn1,mat.n * sizeof (float));
    }

    vector_destroy(&pn);
    vector_destroy(&pn1);

    return delta;

    //TODO: stocker le futur resultat dans la variable adequate
    //page_rank(mat, zero, epsilon);
  }  
}
