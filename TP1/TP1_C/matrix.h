#ifndef __MATRIX_H__
#define __MATRIX_H__

struct matrix {
  int m;
  int n;
  float* C;
  int* L;
  int* I;
};

/*int counter = -1;
  int old_counter;*/

int matrix_m_resize(struct matrix* mat, int new_length);

void matrix_n_resize(struct matrix* mat, int new_length);

int matrix_init(struct matrix* mat);

int matrix_copy(struct matrix* src, struct matrix* dest);

int matrix_parse_from_file(struct matrix* mat, char* filepath);

void matrix_destroy(struct matrix* mat);

#endif
