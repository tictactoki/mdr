#ifndef __PAGE_RANK_H__
#define __PAGE_RANK_H__

#include "matrix.h"
#include "vector.h"

void page_rank_with_zap(struct matrix mat, struct vector init, double factor);

float page_rank(struct matrix mat, struct vector init, float epsilon);

float page_rank_zero(struct matrix mat, int k);

#endif
