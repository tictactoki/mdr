import java.io.File;

public class Matrix {
	private float[] C;
	private int[] L;
	private int[] I;
	private int n, m;

	// TODO calcul C L I
	public Matrix(float[][] matrix) {
		countNotNull(matrix);
		createAllFromMatrix(matrix);
	}

	// TODO creation C L I
	private boolean createAllFromMatrix(float[][] matrix) {

		return false;
	}

	private void countNotNull(float[][] matrix) {
		this.m = 0;
		
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				this.m++;
			}
		}
	}

	public float[] multiplicationMatrix(float vector[]) {
		int length = vector.length;
		float[] P;

		if (length != n) {
			return null;
		}

		P = new float[length];

		for (int i = 0; i < length; i++) {
			for (int j = this.L[i]; j < this.L[i + 1]; j++) {
				P[i] = P[i] + this.C[j] * vector[this.I[j]];
			}
		}

		return P;
	}

	public static Matrix parseFromFile(String filepath) {
		File f = new File(filepath);

		// TODO: creer une matrice a partir du fichier de donnees

		return null;
	}
}
